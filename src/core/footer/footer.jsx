import React, { Component } from 'react'
import './footer.css'

export default class footer extends Component {
  render () {
    return (
      <div className='footer'>
        <a className='link' href='https://github.com/Puex'>
          GitHub
        </a>
        <a className='link' href='https://www.youtube.com/user/OnchayTM'>
          Youtube
        </a>
        <a className='link' href='https://twitter.com/MZPuex'>
          Touitteur
        </a>
        <a className='link' href='http://matthieumontaille.fr'>
          Site
        </a>
        <a className='link' href='https://www.instagram.com/matt_mntl'>
          Instagram
        </a>
        <a className='link' href='https://cestmoijuliettevoyons6.webnode.fr'>
          Blog
        </a>
      </div>
    )
  }
}
