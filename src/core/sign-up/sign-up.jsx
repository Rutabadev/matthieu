import React, { Component } from 'react'
import './sign-up.css'
import MaterialIcon from 'material-icons-react'
import { auth, provider } from '../../firebase/firebase.js'
export default class SignUp extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: null
    }

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
  }

  componentDidMount () {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })
  }

  login () {
    auth.signInWithPopup(provider)
      .then((result) => {
        const user = result.user
        this.setState({
          user
        })
      })
  }

  logout () {
    auth.signOut()
      .then(() => {
        this.setState({
          user: null
        })
      })
  }

  user () {
    if (this.state.user) {
      return (
        <div className='dropdown'>
          <div className='user-profile'>
            <button onClick={this.logout}><img src={this.state.user.photoURL} alt='User profile' /></button>
          </div>
          {/* <div ClassName='dropdown-content'>
            <a href='#' onClick={this.logout}>Log Out</a>
          </div> */}
        </div>
      )
    } else {
      return <button onClick={this.login}><MaterialIcon icon='account_circle' size='medium' /></button>
    }
  }

  render () {
    return (
      <div className='sign-up'>
        {this.user()}
      </div>
    )
  }
}
