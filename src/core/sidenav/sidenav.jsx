import React, { Component } from 'react'
import './sidenav.css'
import {
  Link,
  Route
} from 'react-router-dom'

export default class SideNav extends Component {
  closeNav () {
    const sidenav = this.props.sideNavRef.current
    sidenav.style.width = '0'
  }
  render () {
    const links = []
    const data = this.props.data
    data.forEach(element => {
      links.push(
        <MenuLink key={element.route} to={element.route} label={element.name} classe={element.class} activeOnlyWhenExact={element.route === '/'} />
      )
    })
    return (
      <div ref={this.props.sideNavRef} className='sidenav'>
        <button className='closebtn' onClick={() => this.closeNav()}>&times;</button>
        {links}
      </div>
    )
  }
}

const MenuLink = ({ label, to, classe, activeOnlyWhenExact }) => (
  <Route
    path={to}
    exact={activeOnlyWhenExact}
    children={({ match }) => (
      <li className={classe + (match ? ' active ' : ' ') + 'sidebar-element'}>
        <Link to={to}>{label}</Link>
      </li>
    )}
  />
)
