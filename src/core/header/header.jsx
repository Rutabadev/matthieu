import React, { Component } from 'react'
import './header.css'
import {
  Link,
  Route
} from 'react-router-dom'
import SignUp from '../sign-up/sign-up'
import MaterialIcon from 'material-icons-react'

export default class Header extends Component {
  openNav () {
    const sidenav = this.props.sideNavRef.current
    sidenav.style.width = '250px'
  }

  render () {
    const data = this.props.data
    const links = []
    data.forEach((element) => {
      links.push(
        <MenuLink key={element.route} to={element.route} label={element.name} classe={element.class} activeOnlyWhenExact={element.route === '/'} />
      )
    })

    return (
      <div className='header'>
        <nav className='navbar'>
          <ul className='flex-container flex-start'>
            <div className='menu-icon-container'>
              <div className='menu-icon' onClick={() => this.openNav()}>
                <MaterialIcon icon='menu' size='medium' />
              </div>
            </div>
            <div className='links flex-container'>
              {links}
            </div>
          </ul>
          <div className='flex-container'>
            <SignUp />
          </div>
        </nav>
      </div>
    )
  }
}

const MenuLink = ({ label, to, classe, activeOnlyWhenExact }) => (
  <Route
    path={to}
    exact={activeOnlyWhenExact}
    children={({ match }) => (
      <li className={classe + (match ? ' active ' : ' ') + 'navbar-element'}>
        <Link to={to}>{label}</Link>
      </li>
    )}
  />
)
