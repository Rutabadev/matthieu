import React, { Component } from 'react'
import './404.css'

export default class FourOFour extends Component {
  render () {
    return (
      <div className='fourOfour'>
        <p>Not Found</p>
      </div>
    )
  }
}
