import React, { Component } from 'react'
import './fotos.css'
import matt1 from './assets/matt1.jpg'
import matt2 from './assets/matt2.jpg'
import matt3 from './assets/matt3.jpg'
import matt4 from './assets/matt4.jpg'
import Gallery from 'react-grid-gallery'
import { auth } from '../../firebase/firebase.js'

const IMAGES = [
  {
    src: matt1,
    thumbnail: matt1,
    caption: 'Là il est enveloppé dans une serviette, il devait avoir froid'
  },
  {
    src: matt2,
    thumbnail: matt2,
    caption: 'Pensif, comme rarement'
  },
  {
    src: matt3,
    thumbnail: matt3,
    caption: 'On pourrait croire que ses yeux sont fermés mais que neni'
  },
  {
    src: matt4,
    thumbnail: matt4,
    caption: "Il regarde pas vraiment l'objectif c'est pas cool"
  }
]

export default class Fotos extends Component {
  constructor (props) {
    super()
    this.state = {
      user: null
    }
  }

  componentDidMount () {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })
  }

  render () {
    let component = <NotLoggedIN />
    if (this.state.user) {
      component = <LoggedIn />
    }
    return (
      component
    )
  }
}

const LoggedIn = () => (
  <div className='fotos'>
    <div className='gallery'>
      <Gallery images={IMAGES} />
    </div>
  </div>
)

const NotLoggedIN = () => (
  <p>Sorry, you need to be logged in to see Matthieu's picture.</p>
)
