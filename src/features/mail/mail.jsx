import React, { Component } from 'react'
import './mail.css'

export default class Mail extends Component {
  render () {
    return (
      <div className='mail'>
        <form action='sendMail'>
          <label htmlFor='content'>Content</label>
          <textarea name='content' id='content' cols='30' rows='10' />
        </form>
      </div>
    )
  }
}
