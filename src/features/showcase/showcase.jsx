import React, { Component } from 'react'
import './showcase.css'
import FontAwesome from 'react-fontawesome'
import MaterialIcon from 'material-icons-react'

export default class Showcase extends Component {
  render () {
    return (
      <div className='showcase'>
        <div className='center'>
          <div className='container'>
            <div className='row'>
              <button className='btn btn-primary'>Primary</button>
              <button className='btn btn-secondary'>Secondary</button>
              <button className='btn btn-tertiary'>Tertiary</button>
              <button className='btn btn-warning'>Warning</button>
            </div>
            <div className='row'>
              <FontAwesome
                name='rocket'
                spin
                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }} />
              <MaterialIcon icon='toys' />
            </div>
          </div>
          <br />
        </div>
      </div>
    )
  }
}
