import React, { Component } from 'react'
import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import Header from './core/header/header'
import Footer from './core/footer/footer'
import Home from './features/home/home'
import Showcase from './features/showcase/showcase'
import Fotos from './features/fotos/fotos'
import Mail from './features/mail/mail'
import FourOFour from './core/404/404'
import SideNav from './core/sidenav/sidenav'

const DATA = [
  { name: 'Matthieu', class: 'title link', route: '/' },
  { name: 'Fotos', class: 'link', route: '/fotos' },
  { name: 'CSS Showcase', class: 'link', route: '/showcase' },
  { name: 'Mail', class: 'link', route: '/mail' }
]

class App extends Component {
  constructor (props) {
    super(props)
    this.sideNavRef = React.createRef()
  }

  render () {
    return (
      <Router>
        <div className='app'>
          <SideNav sideNavRef={this.sideNavRef} data={DATA} />
          <Header sideNavRef={this.sideNavRef} data={DATA} />
          <div className='content'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/fotos' component={Fotos} />
              <Route path='/showcase' component={Showcase} />
              <Route path='/mail' component={Mail} />
              <Route path='*' component={FourOFour} />
            </Switch>
          </div>
          <Footer />
        </div>
      </Router>
    )
  }
}

export default App
